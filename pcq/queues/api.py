from .models import CharacterItem, Character


def retrieve_prioritized_queue(creator):
    prioritized_queue_items = CharacterItem.objects.filter(
        creator=creator, status='P').order_by('-priority', 'created_at')

    return prioritized_queue_items

def retrieve_working_item(creator):
    CharacterItem.objects.filter(creator=creator, status='I')
    retrieve_prioritized_queue(creator)


def craft(queue, amount):
    working_item = CharacterItem.objects.get(queue=queue, status='I')
    working_item.completed_price

def calculate_worst_item_dc(
    character=Character.objects.first(),
    character_item=CharacterItem.objects.first()
):
    base_dc = character_item.item
