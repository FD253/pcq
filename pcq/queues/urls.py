from django.conf.urls import url

from ..queues import views

urlpatterns = [
    url(r'^craftingday/new/$', views.new_crafting_day, name='create')
#    url(r'^craftingday/new/$', views.CreateCraftingDayFormView.as_view(),
 #       name='CreateCraftingDayFormView'),
]
