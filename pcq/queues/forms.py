from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Field
from django import forms
from django.core.exceptions import ValidationError
from django.forms import CheckboxSelectMultiple
from docutils.nodes import inline
from .models import Item, CraftingDay, Tool, Character, CharacterSkill, CharacterCraftSkill, CharacterItem, \
    CharacterItemCreationFeat, Class, CharacterClass

EMPTY_ITEM_ERROR = "You can't add an unnamed item"


class ItemForm(forms.ModelForm):

    class Meta:
        model = Item
        fields = ['name']
        widgets = {
            'name': forms.fields.TextInput(attrs={
                'placeholder': 'Enter an item to craft',
                'class': 'form-control input-lg',
            }),
        }
        error_messages = {
            'name': {'required': EMPTY_ITEM_ERROR}
        }

    def save(self, for_queue):
        self.instance.queue = for_queue
        return super().save()


class CraftingDayCrispyForm(forms.ModelForm):
    class Meta:
        model = CraftingDay
        fields = ['block_1', 'block_2', 'accelerating',
                  'skill_to_use', 'adventuring', 'tools',
                  'cooperative_crafting_assistants', 'total']

    def clean(self):
        selected_tools = self.cleaned_data.get('tools')
        block_1 = self.cleaned_data.get('block_1')
        block_2 = self.cleaned_data.get('block_2')
        if selected_tools and len(selected_tools) > 8:
            raise ValidationError('Maximum 8 sets of tools usable per day')
        if selected_tools and len(selected_tools) > 4 and (block_1 or block_2):
            raise ValidationError('The limit of hours dedicated to craft magic items is 8')
        if selected_tools and len(selected_tools) < 5 and (block_1 and block_2):
            raise ValidationError('The limit of hours dedicated to craft magic items is 8')
        adventuring = self.cleaned_data.get('adventuring')
        skill_to_use = self.cleaned_data.get('skill_to_use')
        if (adventuring and block_1) or (adventuring and block_2):
            raise ValidationError('Use time blocks or adventuring, not both')
        if not selected_tools and not block_1 and not block_2 and not adventuring:
            raise ValidationError('You have to use at least one block or select '
                                  'adventuring or use tools (or a valid combination)')
        if (block_1 and not adventuring) or (block_2 and not adventuring) or adventuring:
            if not skill_to_use:
                raise ValidationError('You need to select a craft skill')

        return self.cleaned_data

    def __init__(self, *args, **kwargs):

        super(CraftingDayCrispyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-craftingDayCrispyForm'
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'

        self.helper.add_input(Submit('submit', 'Make (js calc) gp of progress', css_class='btn-block'))
        # Or finish the item if it's the last day needed

        self.fields['tools'] = forms.MultipleChoiceField(
            choices=[(o.id, ' ' + o.skill.crafting_skill.name + ' [' + str(
                int(CharacterCraftSkill.objects.get(character_id=1,
                                                    crafting_skill__name=o.skill.crafting_skill.name).total)+4
            ) + ']') for o in Tool.objects.filter(owner_id=1,
                                                  skill__ranks__gt=5)],
            required=False, widget=CheckboxSelectMultiple, help_text=CraftingDay.HELP_TEXT
        )
        self.fields['skill_to_use'] = forms.ModelChoiceField(
            queryset=CharacterCraftSkill.objects.filter(character_id=1),
            required=False
        )
        self.fields['total'] = forms.IntegerField(required=False)

    def save(self):
        return super().save(commit=False)


from django.forms.models import inlineformset_factory

ccs_formset = inlineformset_factory(Character,
                                    Character.crafting_skills.through,
                                    exclude=('character', 'crafting_skill',),
                                    max_num=1,)
