from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.views.generic import CreateView
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView
from .forms import ItemForm, CraftingDayCrispyForm
from .models import Item, CharacterItem, CraftingDay, Character, CharacterItemCreationFeat, Class, CharacterClass


# Create your views here.


def home_page(request):
    return render(request, "mypages/myhome.html", {'form': ItemForm()})


'''def new_queue(request):
    form = ItemForm(data=request.POST)
    if form.is_valid():
        queue = Queue.objects.create()
        form.save(for_queue=queue)
        return redirect(queue)
    else:
        return render(request, 'mypages/myhome.html', {"form": form})


def view_queue(request, queue_id):
    queue = Queue.objects.get(id=queue_id)
    error = None
    form = ItemForm()

    if request.method == 'POST':
        form = ItemForm(data=request.POST)
        if form.is_valid():
            form.save(for_queue=queue)
            return redirect(queue)
    return render(request, 'mypages/queue.html', {'queue': queue, 'error': error, 'form': form})
'''


def new_crafting_day(request,
                     creator=Character.objects.first(),
                     character_item=CharacterItem.objects.first()):

    form = CraftingDayCrispyForm(data=request.POST)

    def calculate_gp_progress(form):
        creator = Character.objects.first()
        crafting_item = CharacterItem.objects.first()
        used_feat = crafting_item.item.item_creation_feat
        creator_used_feat = CharacterItemCreationFeat.objects.get(character=creator, item_creation_feat=used_feat)

        def add_dwarf_bonus(gp_progress, bonus):
            if creator_used_feat.dwarf_wizard_alternate_favored_class_bonus:
                gp_progress += creator_used_feat.dwarf_wizard_alternate_favored_class_bonus * bonus
            return gp_progress
        gp_progress = 0
        if int(form['block_1'].value()) > 0:
            gp_progress += int(form['block_1'].value())
            gp_progress = add_dwarf_bonus(gp_progress, 100)
        if int(form['block_2'].value()) > 0:
            gp_progress += int(form['block_2'].value())
            gp_progress = add_dwarf_bonus(gp_progress, 100)
        if form['adventuring'].value():
            gp_progress += 250
            gp_progress = add_dwarf_bonus(gp_progress, 50)
        if creator_used_feat.wizard_arcane_builder_discovery:
            gp_progress *= 1.25
        gp_progress += len(form['tools'].value()) * 2000

        if len(creator.class_features.filter(name='asd')) == 1:
            magus = Class.objects.filter(name='Magus')
            if len(magus) == 1:
                creator_magus_info = CharacterClass.objects.get(character=creator, _Class=magus.first())
            if creator_magus_info.level >= 7:
                if used_feat.name == 'Craft Magic Arms and Armor':
                    gp_progress *= 2
        gp_progress *= int(form['cooperative_crafting_assistants'].value()) + 1
        if form['accelerating'].value():
            gp_progress *= 2

        return gp_progress

    if form.is_valid():
        obj = form.save()
        obj.creator = creator
        obj.character_item = character_item
        obj.total = calculate_gp_progress(form)
        obj.save()
        return redirect('home')
    else:
        return render(request, 'queues/craftingday_form.html', {"form": form})




'''class CreateCraftingDayFormView(CreateView):
    model = CraftingDay
    form_class = CraftingDayCrispyForm

    def get_context_data(self, **kwargs):
        ctx = super(CreateCraftingDayFormView, self).get_context_data(**kwargs)
        return ctx

    def post(self, request, *args, **kwargs):
        error = None
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
        return render(request, 'queues/craftingday_form.html', {'error': error, 'form': form, })
'''
