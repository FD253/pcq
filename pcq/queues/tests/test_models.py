from django.core.exceptions import ValidationError
from django.test import TestCase
from ..models import Item, Queue
# Create your tests here.


class QueueModelTest(TestCase):

    def test_get_absolute_url(self):
        queue = Queue.objects.create()
        self.assertEqual(queue.get_absolute_url(), '/queue/%d/' % (queue.id,))


class ItemModelTest(TestCase):

    def test_default_text(self):
        item = Item()
        self.assertEqual(item.text, '')

    def test_item_is_related_to_queue(self):
        queue = Queue.objects.create()
        item = Item()
        item.queue = queue
        item.save()
        self.assertIn(item, queue.item_set.all())

    def test_cannot_save_empty_items(self):
        queue = Queue.objects.create()
        item = Item(queue=queue, text='')
        with self.assertRaises(ValidationError):
            item.save()
            item.full_clean()

