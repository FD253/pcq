from django.http import HttpRequest
from django.test import TestCase
from django.core.urlresolvers import resolve
from django.shortcuts import render
from pcq.queues.forms import ItemForm
from ..views import home_page
from ..models import Item, Queue
from django.utils.html import escape
from unittest import skip
from ..forms import EMPTY_ITEM_ERROR
# Create your tests here.


class HomePageTest(TestCase):
    maxDiff = None

    def test_home_page_renders_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'mypages/myhome.html')

    def test_home_page_uses_item_form(self):
        response = self.client.get('/')
        self.assertIsInstance(response.context['form'], ItemForm)


class NewQueueTest(TestCase):

    def test_saving_a_POST_request(self):
        self.client.post(path='/queue/new/', data={'text': 'A new queue item'})
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new queue item')

    def test_redirects_after_POST(self):
        response = self.client.post(
            '/queue/new/',
            data={'text': 'A new list item'}
        )
        new_queue = Queue.objects.first()
        self.assertRedirects(response, '/queue/%d/' % (new_queue.id,))

    def test_for_invalid_input_renders_home_template(self):
        response = self.client.post('/queue/new/', data={'text': ''})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response='mypages/myhome.html')

    def test_validation_errors_are_sent_back_to_home_page(self):
        response = self.client.post('/queue/new/', data={'text': ''})
        self.assertContains(response, escape(EMPTY_ITEM_ERROR))

    def test_for_invalid_input_passes_form_to_template(self):
        response = self.client.post('/queue/new/', data={'text': ''})
        self.assertIsInstance(response.context['form'], ItemForm)

    def test_invalid_queue_items_arent_saved(self):
        self.client.post('/queue/new/', data={'text': ''})
        self.assertEqual(Queue.objects.count(), 0)
        self.assertEqual(Item.objects.count(), 0)


class QueueViewTest(TestCase):

    def test_uses_queue_template(self):
        queue = Queue.objects.create()
        response = self.client.get('/queue/%d/' % queue.id)
        self.assertTemplateUsed(response, 'mypages/queue.html')

    def test_passes_correct_queue_to_template(self):
        other_queue = Queue.objects.create()
        correct_queue = Queue.objects.create()
        response = self.client.get('/queue/%d/' % correct_queue.id)
        self.assertEqual(response.context['queue'], correct_queue)

    def test_displays_only_items_for_that_queue(self):
        correct_queue = Queue.objects.create()
        Item.objects.create(text='Meridian belt', queue=correct_queue)
        Item.objects.create(text='Ring of counterspells', queue=correct_queue)
        other_queue = Queue.objects.create()
        Item.objects.create(text='Longsword +2', queue=other_queue)
        Item.objects.create(text='Longsword +5', queue=other_queue)

        response = self.client.get('/queue/%d/' % correct_queue.id)
        self.assertContains(response, 'Meridian belt')
        self.assertContains(response, 'Ring of counterspells')
        self.assertNotContains(response, 'Longsword +2')
        self.assertNotContains(response, 'Longsword +5')

    def test_can_save_a_POST_request_to_an_existing_queue(self):
        other_queue = Queue.objects.create()
        correct_queue = Queue.objects.create()

        self.client.post(
            path='/queue/%d/' % (correct_queue.id,),
            data={'text': 'A new item for an existing queue'}
        )

        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new item for an existing queue')
        self.assertEqual(new_item.queue, correct_queue)

    def test_POST_redirects_to_queue_view(self):
        other_queue = Queue.objects.create()
        correct_queue = Queue.objects.create()

        response = self.client.post(
            path='/queue/%d/' % (correct_queue.id,),
            data={'text': 'A new item for an existing queue'}
        )

        self.assertRedirects(response, '/queue/%d/' % correct_queue.id)

    def post_invalid_input(self):
        queue = Queue.objects.create()
        return self.client.post(
            '/queue/%d/' % (queue.id,),
            data={'text': ''}
        )

    def test_for_invalid_input_nothing_saved_to_db(self):
        self.post_invalid_input()
        self.assertEqual(Item.objects.count(), 0)

    def test_for_invalid_input_renders_queue_template(self):
        response = self.post_invalid_input()
        self.assertTemplateUsed(response, 'mypages/queue.html')

    def test_for_invalid_input_passes_form_to_template(self):
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], ItemForm)

    def test_for_invalid_input_shows_error_on_page(self):
        response = self.post_invalid_input()
        self.assertContains(response, escape(EMPTY_ITEM_ERROR))

    def test_displays_item_form(self):
        queue = Queue.objects.create()
        response = self.client.get('/queue/%d/' % (queue.id,))
        self.assertIsInstance(response.context['form'], ItemForm)
        self.assertContains(response, 'name="text"')


