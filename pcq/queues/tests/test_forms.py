from django.test import TestCase
from ..models import Queue, Item

from ..forms import ItemForm, EMPTY_ITEM_ERROR


class ItemFormTest(TestCase):

    def test_form_renders_item_text_input(self):
        form = ItemForm()
        self.assertIn('placeholder="Enter an item to craft"', form.as_p())
        self.assertIn('class="form-control input-lg"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = ItemForm(data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['text'],
            [EMPTY_ITEM_ERROR]
        )

    def test_form_save_handles_saving_to_a_queue(self):
        queue = Queue.objects.create()
        form = ItemForm(data={'text': 'blabla'})
        new_item = form.save(for_queue=queue)
        self.assertEqual(new_item, Item.objects.first())
        self.assertEqual(new_item.text, 'blabla')
        self.assertEqual(new_item.queue, queue)
