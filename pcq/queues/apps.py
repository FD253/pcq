from django.apps import AppConfig


class QueuesConfig(AppConfig):
    name = 'pcq.queues'
