from .forms import CraftingDayCrispyForm
from .models import *

admin.site.register(Party)
admin.site.register(Character, CharacterAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Alignment)
admin.site.register(CharacterClass)
admin.site.register(Spell)
admin.site.register(School)
admin.site.register(Feat)
admin.site.register(ItemCreationFeat)
admin.site.register(Race)
admin.site.register(Class)
admin.site.register(ClassFeature)
admin.site.register(CraftingSkill)
admin.site.register(CharacterItem)
admin.site.register(God)
admin.site.register(Skill)
admin.site.register(Tool)


class CraftingDayAdmin(admin.ModelAdmin):
    form = CraftingDayCrispyForm

admin.site.register(CraftingDay, CraftingDayAdmin)
