from django.contrib import admin
from django.core.urlresolvers import reverse
from django.db import models
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField

from ..users.models import User


# Create your models here.


class Party(models.Model):
    name = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Parties'


class Class(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Classes'


class Alignment(models.Model):
    ALIGNMENT_CHOICES = (
        ('LG', _('Lawful Good')),
        ('NG', _('Neutral Good')),
        ('CG', _('Chaotic Good')),
        ('LN', _('Lawful Neutral')),
        ('NN', _('True Neutral')),
        ('CN', _('Chaotic Neutral')),
        ('LE', _('Lawful Evil')),
        ('NE', _('Neutral Evil')),
        ('CE', _('Chaotic Evil')),
    )
    alignment = models.CharField(_('Alignment'), max_length=2,
                                 choices=ALIGNMENT_CHOICES, unique=True)


class Feat(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class Skill(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class ItemCreationFeat(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class School(models.Model):
    name = models.CharField(max_length=13, unique=True)

    def __str__(self):
        return self.name


class Spell(models.Model):
    name = models.CharField(max_length=30, unique=True)
    school = models.ForeignKey(School)

    def __str__(self):
        return self.name


class Race(models.Model):
    name = models.CharField(max_length=15, unique=True)

    def __str__(self):
        return self.name


class ClassFeature(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class God(models.Model):
    name = models.CharField(max_length=15, unique=True)

    def __str__(self):
        return self.name


class CraftingSkill(models.Model):
    name = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.name


class Character(models.Model):
    player = models.ForeignKey(User)
    name = models.CharField(max_length=30)
    race = models.ManyToManyField(Race, blank=True,
                                  help_text='Example: If half-elf '
                                            'select half-elf, human and elf.')
    _Classes = models.ManyToManyField(Class, through='CharacterClass')
    alignment = models.ForeignKey(Alignment, null=True, blank=True)
    feats = models.ManyToManyField(Feat, blank=True,
                                   help_text="Do not include item creation "
                                             "feats. ")
    hedge_magician_trait = models.BooleanField(blank=True, default=False)
    party = models.ForeignKey(Party, null=True, blank=True)
    item_creation_feats = models.ManyToManyField(
        ItemCreationFeat, through='CharacterItemCreationFeat'
    )
    spells_known = models.ManyToManyField(Spell, blank=True)
    opposed_schools = models.ManyToManyField(School, blank=True)
    caster_level = models.PositiveSmallIntegerField(default=0, blank=True)
    skills = models.ManyToManyField(Skill, through='CharacterSkill',
                                    help_text="Do not include craft skills. ")
    crafting_skills = models.ManyToManyField(CraftingSkill,
                                             through='CharacterCraftSkill')
    worshiper_of = models.ForeignKey(God, null=True, blank=True)
    class_features = models.ManyToManyField(ClassFeature, blank=True)

    def __str__(self):
        return self.name


class CharacterItemCreationFeat(models.Model):
    character = models.ForeignKey(Character)
    item_creation_feat = models.ForeignKey(ItemCreationFeat)
    DWAFCB_CHOICES = [(i, i) for i in range(31)]
    dwarf_wizard_alternate_favored_class_bonus = \
        models.PositiveSmallIntegerField(
            choices=DWAFCB_CHOICES,
            default=0,
            help_text="Times selected as an Alternate Favored Class Bonus. "
                      "Amount of progress made in an 8-hour period increased "
                      "by 200gp * times selected as an AFCB. "
                      "Optional field. Use only with Dwarf Wizard characters."
        )
    wizard_arcane_builder_discovery = models.BooleanField(
        default=False, help_text="This considers the +4 bonus. "
                                 "Do not add it as bonus when setting "
                                 "CharacterCraftingSkills")

    class Meta:
        unique_together = ('character', 'item_creation_feat')


class CharacterItemCreationFeatInline(admin.TabularInline):
    model = CharacterItemCreationFeat
    extra = 1


class CharacterClass(models.Model):
    character = models.ForeignKey(Character)
    _Class = models.ForeignKey(Class)
    LEVEL_CHOICES = [(i, i) for i in range(1, 31)]
    level = models.PositiveSmallIntegerField(choices=LEVEL_CHOICES, default=1)

    class Meta:
        verbose_name_plural = 'Character Classes'
        unique_together = ('character', '_Class')


class CharacterSkill(models.Model):
    character = models.ForeignKey(Character)
    skill = models.ForeignKey(Skill)
    ranks = models.PositiveSmallIntegerField(null=False, blank=False)

    class Meta:
        unique_together = ('character', 'skill')


class CharacterSkillInline(admin.TabularInline):
    model = CharacterSkill
    extra = 1


class CharacterCraftSkill(models.Model):
    character = models.ForeignKey(Character)
    crafting_skill = models.ForeignKey(CraftingSkill)
    RANKS_CHOICES = [(i, i) for i in range(41)]
    ranks = models.PositiveSmallIntegerField(null=False,
                                             blank=False,
                                             default=0,
                                             choices=RANKS_CHOICES)
    class_skill = models.BooleanField(blank=True, default=False)
    ABILITY_MODIFIER_CHOICES = [(i, i) for i in range(-5, 51)]
    ability_modifier = models.SmallIntegerField(
        blank=False, null=False, default=0, choices=ABILITY_MODIFIER_CHOICES
    )
    bonuses_and_penalties = JSONField(
        help_text='Write a JSON valid text. Example: {"skill_focus":3, "trait'
                  '": 1, "negative_levels": -2, "magic_item":3 } . ',
        blank=True, null=True, default={},
    )
    total = models.SmallIntegerField(blank=True)

    def save(self, *args, **kwargs):
        new_total = 0
        new_total += self.ranks
        if self.class_skill:
            new_total += 3
        new_total += self.ability_modifier
        if self.bonuses_and_penalties:
            for k, v in self.bonuses_and_penalties.items():
                try:
                    new_total += v
                except:
                    pass
        self.total = new_total
        super(CharacterCraftSkill, self).save(*args, *kwargs)

    def __str__(self):
        return self.crafting_skill.name + ' [' + str(self.total) + ']'

    class Meta:
        unique_together = ('character', 'crafting_skill')


class CharacterCraftingSkillInline(admin.TabularInline):
    model = CharacterCraftSkill
    extra = 1


class CharacterClassInline(admin.TabularInline):
    model = CharacterClass
    extra = 1


class CharacterAdmin(admin.ModelAdmin):
    inlines = (CharacterClassInline, CharacterCraftingSkillInline,
               CharacterSkillInline, CharacterItemCreationFeatInline)


class Item(models.Model):
    name = models.CharField(max_length=100, unique=True)
    caster_level = models.PositiveSmallIntegerField(null=False, blank=False)
    item_creation_feat = models.ForeignKey(ItemCreationFeat)
    price = models.PositiveIntegerField(null=False, blank=False)
    spells = models.ManyToManyField(Spell, blank=True)
    optional_spell_1 = models.ForeignKey(Spell,
                                         related_name='optional_spell_1',
                                         null=True, blank=True)
    optional_spell_2 = models.ForeignKey(Spell,
                                         related_name='optional_spell_2',
                                         null=True, blank=True)
    _Class = models.ForeignKey(Class, null=True, blank=True)
    race = models.ForeignKey(Race, null=True, blank=True)
    feat = models.ManyToManyField(Feat, blank=True)
    character_level = models.PositiveSmallIntegerField(null=True, blank=True)
    alignment = models.ManyToManyField(Alignment, blank=True)
    worshiper_of = models.ForeignKey(God, null=True, blank=True)
    class_features = models.ManyToManyField(ClassFeature, blank=True)
    skills = models.ManyToManyField(Skill, through='ItemSkill')

    def __str__(self):
        return self.name


class ItemSkill(models.Model):
    item = models.ForeignKey(Item)
    skill = models.ForeignKey(Skill)
    ranks = models.PositiveSmallIntegerField(null=False, blank=False)

    class Meta:
        unique_together = ('item', 'skill')


class ItemSkillInline(admin.TabularInline):
    model = ItemSkill
    extra = 1


class ItemAdmin(admin.ModelAdmin):
    inlines = (ItemSkillInline,)


class CharacterItem(models.Model):
    petitioner = models.ForeignKey(Character, related_name='petitioner')
    creator = models.ForeignKey(Character, related_name='creator')
    item = models.ForeignKey(Item)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    PENDING = 'P'
    IN_PROGRESS = 'I'
    DONE = 'D'
    CANCELLED = 'C'
    STATUS_CHOICES = (
        (PENDING, _('Pending')),
        (IN_PROGRESS, _('In progress')),
        (DONE, _('Done')),
        (CANCELLED, _('Cancelled')),
    )
    status = models.CharField(max_length=1, choices=STATUS_CHOICES,
                              blank=False, default=PENDING)
    PRIORITY_CHOICES = (
        (1, _('Low')),
        (2, _('Middle')),
        (3, _('High')),
        (4, _('Real time')),
    )
    priority = models.PositiveSmallIntegerField(choices=PRIORITY_CHOICES,
                                                default=1)
    completed_price = models.PositiveIntegerField(default=0)

    def verify_needed_item_creation_feats(self):
        if Item.objects.get(id=self.item_id).item_creation_feat in Character.objects.get(id=self.creator_id).item_creation_feats.all():
            return True
        return False

    def save(self):
        if self.verify_needed_item_creation_feats():
            super(CharacterItem, self).save()

        else:
            return redirect('home')


class Tool(models.Model):
    """
    Sets of tools matching the craft skill will add a +4 circumstance bonus
    to the appropriate skill. Don't add the bonus in the JSONField of
    CharacterCraftingSkill
    """
    skill = models.ForeignKey(CharacterCraftSkill)
    owner = models.ForeignKey(Character)


class CraftingDay(models.Model):
    BLOCK_CHOICES = (
        (0, _('Not used')),
        (500, _('Controlled environment: 4hs/500gp')),
        (250, _('Uncontrolled environment: 4hs/250gp')),
    )
    creator = models.ForeignKey(Character)
    character_item = models.ForeignKey(CharacterItem)
    created_at = models.DateTimeField(auto_now_add=True)
    block_1 = models.PositiveSmallIntegerField(choices=BLOCK_CHOICES,
                                               null=True, default=0)
    block_2 = models.PositiveSmallIntegerField(choices=BLOCK_CHOICES,
                                               null=True, default=0)
    skill_to_use = models.ForeignKey(CharacterCraftSkill,
                                     null=True, blank=True)
    accelerating = models.BooleanField(default=False)
    adventuring = models.BooleanField(default=False)
    COOPERATIVE_CRAFTING_ASSISTANTS_CHOICES = [(i, i) for i in range(201)]
    cooperative_crafting_assistants = models.PositiveSmallIntegerField(
        choices=COOPERATIVE_CRAFTING_ASSISTANTS_CHOICES,
        default=0, help_text="This option doubles the progress per assistant "
                             "and adds +2 circumstance bonus per assistant "
                             "(stacks) to the check. Use only if the "
                             "assistants has the appropriate feats to help. "
    )
    HELP_TEXT = "Use only if the skill is suitable to craft the item. " \
                "The 2000gp (max per 1 hour) that grants the tools " \
                "can be modified by other options like accelerated crafting " \
                "and/or cooperative crafting. Do not include the +4 " \
                "circumstance bonus when adding crafting skills to the " \
                "character unless it has less than 6 ranks in the " \
                "appropriate skill. "
    tools = models.ManyToManyField(Tool, help_text=HELP_TEXT, blank=True,)
    total = models.PositiveIntegerField(default=0)


class CharacterItemInline(admin.TabularInline):
    model = CharacterItem

