from unittest import skip
from .base import FunctionalTest


class QueueValidationTest(FunctionalTest):

    def get_error_element(self):
        return self.browser.find_element_by_css_selector('.has-danger')

    def test_cannot_add_empty_queue_item(self):
        # Some user goes to the home page and accidentally tries to submit
        # an empty queue item. Hits Enter on the empty input box
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('\n')

        # The home page refreshes, and there is an error message saying
        # that queue items cannot be blank
        error = self.get_error_element()
        self.assertEqual(error.text, "You can't add an unnamed item")

        # Tries again with some text for the item, which now works
        self.get_item_input_box().send_keys('Ring of protection +1\n')
        self.check_for_row_in_queue_table('1: Ring of protection +1')

        # Perversely, she now decides to submit a second blank queue item
        self.get_item_input_box().send_keys('\n')

        # She receives a similar warning on the queue page
        self.check_for_row_in_queue_table('1: Ring of protection +1')
        error = self.get_error_element()
        self.assertEqual(error.text, "You can't add an unnamed item")

        # And she can correct it by filling some text in
        self.get_item_input_box().send_keys('Belt of giant strength +2\n')
        self.check_for_row_in_queue_table('1: Ring of protection +1')
        self.check_for_row_in_queue_table('2: Belt of giant strength +2')

    def test_error_messages_are_cleared_on_input(self):
        # User start new queue with an unnamed item
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('\n')
        error = self.get_error_element()
        self.assertTrue(error.is_displayed())

        # User starts typing in the input box to clear the error
        self.get_item_input_box().send_keys('a')

        # User is pleased to see that the error message disappears
        error = self.get_error_element()
        self.assertFalse(error.is_displayed())

    def append_to_queue(self):
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('Ring of protection +1\n')

    def test_error_messages_are_cleared_on_input_after_successful_append_to_queue(self):
        self.append_to_queue()
        # User start new queue with an unnamed item
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('\n')
        error = self.get_error_element()
        self.assertTrue(error.is_displayed())

        # User starts typing in the input box to clear the error
        self.get_item_input_box().send_keys('a')

        # User is pleased to see that the error message disappears
        error = self.get_error_element()
        self.assertFalse(error.is_displayed())

    def test_error_messages_are_cleared_on_select(self):
        # User start new queue with an unnamed item
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('\n')
        error = self.get_error_element()
        self.assertTrue(error.is_displayed())

        # User selects the input box to clear the error
        self.get_item_input_box().click()

        # User is pleased to see that the error message disappears
        error = self.get_error_element()
        self.assertFalse(error.is_displayed())

    def test_error_messages_are_cleared_on_select_after_sucessful_append_to_queue(self):
        self.append_to_queue()
        # User start new queue with an unnamed item
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('\n')
        error = self.get_error_element()
        self.assertTrue(error.is_displayed())

        # User selects the input box to clear the error
        self.get_item_input_box().click()

        # User is pleased to see that the error message disappears
        error = self.get_error_element()
        self.assertFalse(error.is_displayed())
