from .base import FunctionalTest


class LayoutAndStylingTest(FunctionalTest):

    def test_layout_and_styling(self):  # Erratic behaviour
        # User1 goes to the home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        # The input box is centered
        inputbox = self.get_item_input_box()

        # Without this throws an error
        a = inputbox.location['x']
        b = inputbox.size['width']
        print(a, b, a+(b/2))

        self.assertAlmostEqual(
            first=inputbox.location['x'] + (inputbox.size['width'] / 2),
            second=512,
            delta=5,
        )

        # The user starts a new queue and sees the input centered too
        inputbox.send_keys('testing\n')

        self.assertAlmostEqual(
            a+b/2, 512, delta=5
        )

