from .base import FunctionalTest

import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(FunctionalTest):

    def test_can_start_a_queue_and_retrieve_it_later(self):
        # Check home
        self.browser.get(self.live_server_url)

        # Page title and header pcq
        self.assertIn('pcq', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Pathfinder Crafting Queue', header_text)

        # Add item
        inputbox = self.get_item_input_box()
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter an item to craft'
        )

        # Types 'Ring of sustenance' into a text box
        inputbox.send_keys('Ring of sustenance')

        # Press enter and the user is redirected to other URL
        # The page lists "1: Ring of sustenance"
        inputbox.send_keys(Keys.ENTER)
        taeral_queue_url = self.browser.current_url
        self.assertRegex(taeral_queue_url, '/queue/.+')
        self.check_for_row_in_queue_table('1: Ring of sustenance')

        # There is still a text box for adding new items
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Handy haversack')
        inputbox.send_keys(Keys.ENTER)

        # The page updates again, and now shows both items on Taeral queue
        self.check_for_row_in_queue_table('1: Ring of sustenance')
        self.check_for_row_in_queue_table('2: Handy haversack')

        # Now a new user comes to the site
        self.browser.quit()
        self.browser = webdriver.Chrome(os.environ['CHROMEDRIVER'])

        # John (?) visits the page but there is no sign of Taeral queue
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Ring of sustenance', page_text)
        self.assertNotIn('haversack', page_text)

        # John creates his queue by entering a new item
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Holy avenger')  # Lucky bastard
        inputbox.send_keys(Keys.ENTER)

        # John gets his unique URL
        john_queue_url = self.browser.current_url
        self.assertRegex(john_queue_url, '/queue/.+')
        self.assertNotEqual(john_queue_url, taeral_queue_url)

        # Again, there is no trace of Taeral queue
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Ring of sustenance', page_text)
        self.assertIn('Holy avenger', page_text)

        # Satisfied, they both go back to sleep...
        # Well only John, because Taeral is an elf (?)

